#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/cdev.h>
#include<linux/skbuff.h>
#include<linux/netdevice.h>
#include <linux/tcp.h>
#include <linux/ip.h>

#define NIPQUAD(addr) \
    ((unsigned char *)&addr)[0], \
    ((unsigned char *)&addr)[1], \
    ((unsigned char *)&addr)[2], \
    ((unsigned char *)&addr)[3]


int pack_rcv( struct sk_buff *skb, struct net_device *dev, struct packet_type *pt,struct net_device *odev ) { 

	struct iphdr *iph; 
	struct tcphdr *tcph;
    char *user_data_ptr, *tail, *i; 

    iph = ip_hdr(skb); /*получаем IP заголовок*/
    if( iph->protocol==IPPROTO_TCP )
    {
		tcph = tcp_hdr(skb); /*получаем TCP заголовок*/
		user_data_ptr = (char *)((char *)tcph + (tcph->doff * 4)); /*получаем адрес начала полезных данных, doff - это длина заголовка tcp*/
		tail = skb_tail_pointer(skb); /*получаем адрес конца полезных данных*/
        printk("Пришел новый пакет: \n");
        printk("    Protocol: TCP\n");    
        printk("    Размер данных:%u\n", skb->len);            	
        printk("    Данные\n");
		for (i = user_data_ptr; i != tail; ++i)
			printk("%c", *(char *)i); /*вывод полезных данных*/
        printk("\n    Конец данных\n");
        printk("    Источник: %d.%d.%d.%d\n",NIPQUAD(iph->saddr));        
    }

    return 0;
};

static struct packet_type packet_t = { 
	.type = __constant_htons(ETH_P_IP),
	.func = pack_rcv,
};

static int __init my_init( void ) 
{ 
    dev_add_pack( &packet_t ); 
    printk( KERN_ALERT "TCP recieving module loaded\n" ); 
    return 0; 
} 
	
static void __exit my_exit( void ) 
{ 
    dev_remove_pack( &packet_t ); 
    printk( KERN_ALERT "ICMP recieving module unloaded\n" ); 
} 
	
module_init( my_init ); 
module_exit( my_exit );

#define AUTHOR "Asmi <laa2746@gmail.com>"
#define DESCRIPTION   " Simple tcp packet recieving program"
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_LICENSE("GPL");
