CURRENT = $(shell uname -r) 
KDIR = /usr/src/linux-headers-$(CURRENT)/
PWD = $(shell pwd) 
obj-m   := icmp.o tcp.o
default: 
	$(MAKE) -C $(KDIR) M=$(PWD) modules 
clean: 
	@rm -f *.o .*.cmd .*.flags *.mod.c *.order 
	@rm -f .*.*.cmd *~ *.*~ TODO.* 
	@rm -fR .tmp* 
	@rm -rf .tmp_versions 
disclean: clean 
	@rm *.ko *.symvers
