#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/cdev.h>
#include<linux/skbuff.h>
#include<linux/netdevice.h>
#include <linux/ip.h>
#include <linux/icmp.h>

#define NIPQUAD(addr) \
    ((unsigned char *)&addr)[0], \
    ((unsigned char *)&addr)[1], \
    ((unsigned char *)&addr)[2], \
    ((unsigned char *)&addr)[3]


int pack_rcv( struct sk_buff *skb, struct net_device *dev, struct packet_type *pt,struct net_device *odev ) { 

	struct iphdr *iph; 
	struct icmphdr *icmphdr;
    char *user_data_ptr, *tail, *i; 

    iph = ip_hdr(skb); /*получаем IP заголовок*/
    if( iph->protocol==IPPROTO_ICMP )
    {
		icmphdr = (struct icmphdr *)(skb->data + sizeof(struct iphdr)); /*получаем ICMP заголовок*/
		user_data_ptr = (char *)((char *)icmphdr + 4); /*получаем адрес начала полезных данных*/
		tail = skb_tail_pointer(skb); /*получаем адрес конца полезных данных*/
		
        printk("Пришел новый пакет: \n");
        printk("    Источник: %d.%d.%d.%d\n",NIPQUAD(iph->saddr));
        printk("    Protocol: ICMP\n"); 
        printk("    Тип: %u\n", icmphdr->type);  
        printk("    Код: %u\n", icmphdr->code);    
        printk("    Размер данных:%u\n", skb->len);            	
        printk("    Данные\n");
		for (i = user_data_ptr; i != tail; ++i)
			printk("%x ", *(char *)i); /*данные выводим в hex*/
        printk("\n    Конец данных\n");        
    }

    return 0;
};

static struct packet_type packet_t = { 
	.type = __constant_htons(ETH_P_IP),
	.func = pack_rcv,
};

static int __init my_init( void ) 
{ 
    dev_add_pack( &packet_t ); 
    printk( KERN_ALERT "ICMP recieving module loaded\n" ); 
    return 0; 
} 
	
static void __exit my_exit( void ) 
{ 
    dev_remove_pack( &packet_t ); 
    printk( KERN_ALERT "ICMP recieving module unloaded\n" ); 
} 
	
module_init( my_init ); 
module_exit( my_exit );

#define AUTHOR "Asmi <laa2746@gmail.com>"
#define DESCRIPTION   " Simple icmp packet recieving program"
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_LICENSE("GPL");
